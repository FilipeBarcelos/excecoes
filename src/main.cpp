#include <iostream>

using namespace std;

int testaExcecao() {
	int valor;
	cout << "Insira um valor maior que 0: ";
	cin >> valor;
	if(valor <= 0) {
		//throw(1); // Exceção do tipo int
		throw('a'); // Exceção do tipo char
		//throw(10.55f); // Exceção do tipo float
	}
	return valor;
}

int main(int argc, char ** argv) {
	int valor;
	try {
	valor = testaExcecao();
	cout << "Valor = " << valor << endl;
	}
	catch(int caralho) {
		cout << "Exceção Gerada. Entrada Inválida int." << endl;
	} 
        catch(char mito) {
                cout << "Exceção Gerada. Entrada Inválida char." << endl;
        }
        catch(float pirocudo) {
                cout << "Exceção Gerada. Entrada Inválida float." << endl;
        } 
        catch(...) {
                cout << "Exceção Gerada. Entrada Inválida de qualquer coisa." << endl;
        }

return 0;

}
